package jarvis.java.fraudolent;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class Log {

	private String logfile;
	private FileWriter logWriter;
	
	Log(String logfile) throws IOException
	{
		this.logfile = logfile;
		this.logWriter = new FileWriter(logfile, true);
	}
	
	public void log(String message) throws IOException
	{
		logWriter.append(String.format("%s, %s\n", Instant.now().toString(), message));
		logWriter.flush();
	}
	
	public void close() throws IOException
	{
		logWriter.close();
	}
	
}
