package jarvis.java.fraudolent;

import java.io.IOException;
import java.util.Hashtable;

public class Main {

	public static void main(String[] args) {
		Hashtable<String, Compagnia> compagnie = new Hashtable<String, Compagnia>();
		try {
			ScannerDaFile scannerDati = new ScannerDaFile("dati.txt");
			Log log = new Log("registro");
			String line = "";
			do {
				Compagnia c;
				line = scannerDati.readLine();
				if (line == null) {
					break;
				}
				String operazione[] = line.split(" ");
				String compagnia = operazione[0];
				double valoreAzione = Double.parseDouble(operazione[1]);
				int numeroAzioni = Integer.parseInt(operazione[2]);
				String tipoOperazione = operazione[3];
				if (!compagnie.containsKey(compagnia)) {
					c = new Compagnia(compagnia);
					compagnie.put(compagnia, c);
				} else {
					c = compagnie.get(compagnia);
				}
				if (tipoOperazione.equals("B")) {
					c.costoAquisti += numeroAzioni * valoreAzione;
				} else {
					c.guadagnoVendite += numeroAzioni * valoreAzione;
				}
				c.scambitotali++;	
			} while (true);
			for (Compagnia c : compagnie.values()) {
				log.log(String.format("%s OP: %d BUY: %f SELL: %f", c.nome, c.scambitotali, c.costoAquisti, c.guadagnoVendite));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
