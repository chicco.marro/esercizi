package jarvis.java.fraudolent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Scanner {

	BufferedReader reader;
	
	Scanner()
	{
		InputStream ins = System.in;
		Reader r = new InputStreamReader(ins);
		reader = new BufferedReader(r);
		
//		reader = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public String readLine() throws IOException
	{
		return reader.readLine();
	}
	
}
