package jarvis.java.fraudolent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

// legge le righe da un file di testo
// le righe sono separata da \n (line feed)
public class ScannerDaFile extends Scanner {
	
	ScannerDaFile(String file) throws FileNotFoundException
	{
		// crea un file reader a partire dal file dato
		FileReader fileReader = new FileReader(file);
		
		// crea il buffered reader per poter leggere il contenuto una riga alla volta
		reader = new BufferedReader(fileReader);
	}
	

}
